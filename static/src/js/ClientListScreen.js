odoo.define('l10n_cl_dte_point_of_sale.ClientListScreen', function (require) {
"use strict";

var ClientListScreen = require('point_of_sale.ClientListScreen');
var core = require('web.core');
var QWeb = core.qweb;
var _t = core._t;
var rpc = require('web.rpc');
const Registries = require('point_of_sale.Registries');


const FEClientListScreen = (ClientListScreen) =>
		class extends ClientListScreen {
			constructor(){
				super(...arguments);
			}
			back(){
				if(this.state.detailIsShown) {
					this.state.editModeProps.partner = {
						country_id: this.env.pos.company.country_id,
						city_id: this.env.pos.company.city_id,
						city: this.env.pos.company.city,
						state_id: this.env.pos.company.state_id,
					}
				}
				super.back();
			}
		}
	Registries.Component.extend(ClientListScreen, FEClientListScreen);

	return FEClientListScreen;
});
