from odoo import SUPERUSER_ID, api
import logging


_logger = logging.getLogger(__name__)


def migrate(cr, installed_version):
    _logger.warning("Post Migrating l10n_cl_dte_point_of_sale from version %s to 14.0.0.30.1" % installed_version)

    env = api.Environment(cr, SUPERUSER_ID, {})
    for r in env['dte.caf'].search([('document_class_id.sii_code', 'in', [39, 41, 61])]):
        r.compute_folio_actual()
